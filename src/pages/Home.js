import axios from "axios";
import { useEffect, useState } from "react";
import { Amount } from "../component/Amount";
import { FormAmount } from "../component/FormAmount";
import { Month } from "../component/Month";
import "./Home.css"

export default function Home() {
    const [amounts, setAmounts] = useState([]);

    async function fetchAmount() {
        const response = await axios.get(process.env.REACT_APP_SERVER_URL+ '/api/budget/budgets');

        setAmounts(response.data);
    }
    
    // useEffect(() => {
    //     fetchAmount();
    // }, [fetchAmount]);

    useEffect(() => {
        fetchAmount();
    }, []);

    async function deleteAmount(id){
        await axios.delete(process.env.REACT_APP_SERVER_URL+ '/api/budget/delete/'+id)
        setAmounts(
            amounts.filter(item => item.id !== id)
        );
    }
    async function addAmount(amount){
        const response = await axios.post(process.env.REACT_APP_SERVER_URL+'/api/budget/add', amount)
        setAmounts([
                ...amounts,
                response.data
            ])
    }
    function Price(){
        let total = 0;
        for (const element of amounts) {
            total += element.amount
            
            
        }return total
    }
    async function fetchMonth(month){
        const response = await axios.get(process.env.REACT_APP_SERVER_URL+ "/api/budget/month/" + month)
        setAmounts(response.data)
    }

    return (
        <div className="page">
            <h1>Your Budget List</h1>
            <Month reset={fetchAmount} selectMonth={fetchMonth}/>
            
            
            
            
            <div className="contour">

            {amounts.map(item => <Amount key={item.id} amount={item} onDelete={deleteAmount}/>)}
            
            <p className="total"> Total :<span>{Number.parseFloat(Price()).toFixed(2)}€</span> </p>
            
            
            
            
        </div>
        <FormAmount onFormSubmit={addAmount} />
        </div>
    )

}