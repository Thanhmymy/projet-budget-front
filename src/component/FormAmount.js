import { useState } from "react";
import "./FormAmount.css"


const initialState = {
    category:'',
    date:'',
    amount:''
}

export function FormAmount({onFormSubmit}){
    const [form, setForm]= useState(initialState)
    
    function handleChange(event){
        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }

    function handleSubmit(event){
        event.preventDefault();
        onFormSubmit(form)
    }
    return(
        <div>
            <br />
            <br />
        <h2>Add a budget</h2>
        <div className="form">
        <form onSubmit={handleSubmit}>
            <label>Category:</label>
            <input type="text" name="category" onChange={handleChange} value={form.category}/>
            <label>Date:</label>
            <input type="date" name="date" onChange={handleChange} value={form.date}/>
            <label>Amount:</label>
            <input type="number" name="amount" onChange={handleChange} value={form.amount}/>
            <button>Submit</button>
        </form>
        </div>
        </div>
    )
}