import "./Amount.css"

export function Amount({ amount, onDelete }) {
    return (
        <>
        <div>
        <p>Budget n°{amount.id} : {amount.category} <span>{amount.date} </span>{Number.parseFloat(amount.amount).toFixed(2)}€
            <button onClick={() => onDelete(amount.id)}>X</button>
        </p>
        
        </div>
        
</>
    )
}