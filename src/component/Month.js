export function Month({reset, selectMonth}) {

    function handleReset(){
        reset()
    }

    function handleSelect(month){
        selectMonth(month)
    }
    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    function getMonthsForLocale(locale) {
        var format = new Intl.DateTimeFormat(locale, { month: 'long' })
        var months = []
        for (var month = 0; month < 12; month++) {
            var testDate = new Date(Date.UTC(2000, month, 1, 0, 0, 0));
            months.push(capitalizeFirstLetter(format.format(testDate)))
        }
        return months;
    }
    
    const months = getMonthsForLocale('fr-FR')


    return (
        <>
            <div className="dropdown">
                <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Mois
                </button>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <p className="dropdown-item" onClick={handleReset}>Reset</p>
                    {months.map((month,index)=><p key={index} className="dropdown-item" onClick={()=>handleSelect(index+1)}>{month}</p>)}
                </div>
            </div>
        </>
    )
}