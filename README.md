# Projet Budget

Ce projet est un projet individuel, de 2 semaines, pendant la formation Developpeur Web et Web Mobile.

## Langage utilisé 

React.js
Node.js

## Ce qu'on utilisé dans le JavaScript

- Express Server
- Rooter
- Supertest
- Dotenv-flow

## Objectif 

1. Faire la conception d'une application de gestion de budget avec un back-end et un front en React.js
 
![alt text](public/Assets/2021-07-12%20(2).png)

## Organisation

Tout d'abord, j'ai commencé a créer un dépot git pour mon back et mon front.
Le dépot de mon back : [BackEnd](https://gitlab.com/Thanhmymy/projet-budjet)

Pendant-ce projet, j'ai suivis le schéma ci dessus pour developper mon application.


